Antenna<Antenna>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.sense.uesInfo.antenna.repcap_antenna_get()
	driver.sense.uesInfo.antenna.repcap_antenna_set(repcap.Antenna.Nr1)





.. autoclass:: RsCmwWlanSig.Implementations.Sense.UesInfo.Antenna.AntennaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.uesInfo.antenna.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UesInfo_Antenna_ArxbPower.rst