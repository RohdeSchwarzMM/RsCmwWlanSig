Eattenuation
----------------------------------------





.. autoclass:: RsCmwWlanSig.Implementations.Configure.RfSettings.Antenna.Eattenuation.EattenuationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.antenna.eattenuation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Antenna_Eattenuation_InputPy.rst
	Configure_RfSettings_Antenna_Eattenuation_Output.rst