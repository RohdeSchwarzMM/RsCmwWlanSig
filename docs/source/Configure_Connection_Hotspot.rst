Hotspot
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:HOTSpot:HSSPar
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:HOTSpot:MNDigits
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:HOTSpot:HSPar

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:HOTSpot:HSSPar
	CONFigure:WLAN:SIGNaling<instance>:CONNection:HOTSpot:MNDigits
	CONFigure:WLAN:SIGNaling<instance>:CONNection:HOTSpot:HSPar



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Hotspot.HotspotCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.hotspot.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Hotspot_Cutil.rst
	Configure_Connection_Hotspot_Dname.rst
	Configure_Connection_Hotspot_Plmn.rst
	Configure_Connection_Hotspot_Realm.rst