Connection
----------------------------------------





.. autoclass:: RsCmwWlanSig.Implementations.Configure.Sta.Connection.ConnectionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.sta.connection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Sta_Connection_Ampdu.rst
	Configure_Sta_Connection_Dfdef.rst
	Configure_Sta_Connection_Hetf.rst
	Configure_Sta_Connection_Qos.rst