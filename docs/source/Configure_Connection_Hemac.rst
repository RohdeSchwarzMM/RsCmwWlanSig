Hemac
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:HEMac:BSRSupport

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:HEMac:BSRSupport



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Hemac.HemacCls
	:members:
	:undoc-members:
	:noindex: