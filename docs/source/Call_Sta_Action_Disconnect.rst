Disconnect
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALL:WLAN:SIGNaling<Instance>:STA<s>:ACTion:DISConnect

.. code-block:: python

	CALL:WLAN:SIGNaling<Instance>:STA<s>:ACTion:DISConnect



.. autoclass:: RsCmwWlanSig.Implementations.Call.Sta.Action.Disconnect.DisconnectCls
	:members:
	:undoc-members:
	:noindex: