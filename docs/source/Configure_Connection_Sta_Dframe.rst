Dframe
----------------------------------------





.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Sta.Dframe.DframeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.sta.dframe.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Sta_Dframe_Hemu.rst