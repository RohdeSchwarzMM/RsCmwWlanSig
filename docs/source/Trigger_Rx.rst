Rx
----------------------------------------





.. autoclass:: RsCmwWlanSig.Implementations.Trigger.Rx.RxCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.rx.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Rx_MacFrame.rst