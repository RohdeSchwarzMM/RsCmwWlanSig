Srates
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:SRATes:VHTConf
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:SRATes:OMCSconf
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:SRATes:OFDMconf
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:SRATes

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:SRATes:VHTConf
	CONFigure:WLAN:SIGNaling<instance>:CONNection:SRATes:OMCSconf
	CONFigure:WLAN:SIGNaling<instance>:CONNection:SRATes:OFDMconf
	CONFigure:WLAN:SIGNaling<instance>:CONNection:SRATes



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Srates.SratesCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.srates.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Srates_DsssConf.rst