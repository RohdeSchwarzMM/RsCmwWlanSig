BlAllocation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:PER:DFRame:HEMU:BLALlocation

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:PER:DFRame:HEMU:BLALlocation



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Per.Dframe.Hemu.BlAllocation.BlAllocationCls
	:members:
	:undoc-members:
	:noindex: