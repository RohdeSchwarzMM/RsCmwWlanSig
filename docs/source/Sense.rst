Sense
----------------------------------------





.. autoclass:: RsCmwWlanSig.Implementations.Sense.SenseCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Elogging.rst
	Sense_Pgen.rst
	Sense_Sinfo.rst
	Sense_Sta.rst
	Sense_UesInfo.rst