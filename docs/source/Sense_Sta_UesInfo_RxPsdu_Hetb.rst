Hetb
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:WLAN:SIGNaling<instance>:STA<s>:UESinfo:RXPSdu:HETB

.. code-block:: python

	SENSe:WLAN:SIGNaling<instance>:STA<s>:UESinfo:RXPSdu:HETB



.. autoclass:: RsCmwWlanSig.Implementations.Sense.Sta.UesInfo.RxPsdu.Hetb.HetbCls
	:members:
	:undoc-members:
	:noindex: