UeAddress<IpVersion>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: V4 .. V6
	rc = driver.sense.sta.uesInfo.ueAddress.repcap_ipVersion_get()
	driver.sense.sta.uesInfo.ueAddress.repcap_ipVersion_set(repcap.IpVersion.V4)





.. autoclass:: RsCmwWlanSig.Implementations.Sense.Sta.UesInfo.UeAddress.UeAddressCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.sta.uesInfo.ueAddress.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Sta_UesInfo_UeAddress_Ipv.rst