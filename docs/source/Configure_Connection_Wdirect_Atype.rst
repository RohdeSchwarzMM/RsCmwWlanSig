Atype
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:WDIRect:ATYPe

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:WDIRect:ATYPe



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Wdirect.Atype.AtypeCls
	:members:
	:undoc-members:
	:noindex: