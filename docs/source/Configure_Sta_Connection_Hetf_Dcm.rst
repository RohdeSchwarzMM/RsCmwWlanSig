Dcm
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:STA<s>:CONNection:HETF:DCM

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:STA<s>:CONNection:HETF:DCM



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Sta.Connection.Hetf.Dcm.DcmCls
	:members:
	:undoc-members:
	:noindex: