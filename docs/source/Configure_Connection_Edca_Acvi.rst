Acvi
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:EDCA:ACVI

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:EDCA:ACVI



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Edca.Acvi.AcviCls
	:members:
	:undoc-members:
	:noindex: