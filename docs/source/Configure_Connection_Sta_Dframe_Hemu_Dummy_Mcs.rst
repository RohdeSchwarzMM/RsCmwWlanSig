Mcs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:STA<s>:DFRame:HEMU:DUMMy<index>:MCS

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:STA<s>:DFRame:HEMU:DUMMy<index>:MCS



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Sta.Dframe.Hemu.Dummy.Mcs.McsCls
	:members:
	:undoc-members:
	:noindex: