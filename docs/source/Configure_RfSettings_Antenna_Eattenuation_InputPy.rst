InputPy
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:RFSettings:ANTenna<n>:EATTenuation:INPut

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:RFSettings:ANTenna<n>:EATTenuation:INPut



.. autoclass:: RsCmwWlanSig.Implementations.Configure.RfSettings.Antenna.Eattenuation.InputPy.InputPyCls
	:members:
	:undoc-members:
	:noindex: