Dummy<Dummy>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr3
	rc = driver.configure.connection.sta.dframe.hemu.dummy.repcap_dummy_get()
	driver.configure.connection.sta.dframe.hemu.dummy.repcap_dummy_set(repcap.Dummy.Nr1)





.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Sta.Dframe.Hemu.Dummy.DummyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.sta.dframe.hemu.dummy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Sta_Dframe_Hemu_Dummy_Mcs.rst