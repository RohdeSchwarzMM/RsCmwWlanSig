Ctype
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:PER:DFRame:HEMU:USER<index>:CTYPe

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:PER:DFRame:HEMU:USER<index>:CTYPe



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Per.Dframe.Hemu.User.Ctype.CtypeCls
	:members:
	:undoc-members:
	:noindex: