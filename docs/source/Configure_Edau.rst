Edau
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:EDAU:NID
	single: CONFigure:WLAN:SIGNaling<instance>:EDAU:NSEGment
	single: CONFigure:WLAN:SIGNaling<instance>:EDAU:ENABle

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:EDAU:NID
	CONFigure:WLAN:SIGNaling<instance>:EDAU:NSEGment
	CONFigure:WLAN:SIGNaling<instance>:EDAU:ENABle



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Edau.EdauCls
	:members:
	:undoc-members:
	:noindex: