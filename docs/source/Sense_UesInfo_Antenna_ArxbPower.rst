ArxbPower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:WLAN:SIGNaling<instance>:UESinfo[:ANTenna<n>]:ARXBpower

.. code-block:: python

	SENSe:WLAN:SIGNaling<instance>:UESinfo[:ANTenna<n>]:ARXBpower



.. autoclass:: RsCmwWlanSig.Implementations.Sense.UesInfo.Antenna.ArxbPower.ArxbPowerCls
	:members:
	:undoc-members:
	:noindex: