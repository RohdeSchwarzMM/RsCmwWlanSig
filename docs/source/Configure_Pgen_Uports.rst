Uports
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:PGEN<index>:UPORts

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:PGEN<index>:UPORts



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Pgen.Uports.UportsCls
	:members:
	:undoc-members:
	:noindex: