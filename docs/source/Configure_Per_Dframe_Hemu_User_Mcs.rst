Mcs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:PER:DFRame:HEMU:USER<index>:MCS

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:PER:DFRame:HEMU:USER<index>:MCS



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Per.Dframe.Hemu.User.Mcs.McsCls
	:members:
	:undoc-members:
	:noindex: