Plength
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:WLAN:SIGNaling<instance>:RX:MACFrame:PLENgth:MODE
	single: TRIGger:WLAN:SIGNaling<instance>:RX:MACFrame:PLENgth:VALue

.. code-block:: python

	TRIGger:WLAN:SIGNaling<instance>:RX:MACFrame:PLENgth:MODE
	TRIGger:WLAN:SIGNaling<instance>:RX:MACFrame:PLENgth:VALue



.. autoclass:: RsCmwWlanSig.Implementations.Trigger.Rx.MacFrame.Plength.PlengthCls
	:members:
	:undoc-members:
	:noindex: