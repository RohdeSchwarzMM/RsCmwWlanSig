IpvSix
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:IPVSix:PREFix

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:IPVSix:PREFix



.. autoclass:: RsCmwWlanSig.Implementations.Configure.IpvSix.IpvSixCls
	:members:
	:undoc-members:
	:noindex: