OobDiscovery
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:OOBDiscovery:ENABle
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:OOBDiscovery:SSID
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:OOBDiscovery:BSSid
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:OOBDiscovery:CHANnel
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:OOBDiscovery:OPCLass
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:OOBDiscovery:PSD
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:OOBDiscovery:PROBeresp
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:OOBDiscovery:BROadcast

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:OOBDiscovery:ENABle
	CONFigure:WLAN:SIGNaling<instance>:CONNection:OOBDiscovery:SSID
	CONFigure:WLAN:SIGNaling<instance>:CONNection:OOBDiscovery:BSSid
	CONFigure:WLAN:SIGNaling<instance>:CONNection:OOBDiscovery:CHANnel
	CONFigure:WLAN:SIGNaling<instance>:CONNection:OOBDiscovery:OPCLass
	CONFigure:WLAN:SIGNaling<instance>:CONNection:OOBDiscovery:PSD
	CONFigure:WLAN:SIGNaling<instance>:CONNection:OOBDiscovery:PROBeresp
	CONFigure:WLAN:SIGNaling<instance>:CONNection:OOBDiscovery:BROadcast



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.OobDiscovery.OobDiscoveryCls
	:members:
	:undoc-members:
	:noindex: