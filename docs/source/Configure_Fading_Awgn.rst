Awgn
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:FADing:AWGN:ENABle
	single: CONFigure:WLAN:SIGNaling<instance>:FADing:AWGN:SNRatio

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:FADing:AWGN:ENABle
	CONFigure:WLAN:SIGNaling<instance>:FADing:AWGN:SNRatio



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Fading.Awgn.AwgnCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.fading.awgn.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Fading_Awgn_Bandwidth.rst