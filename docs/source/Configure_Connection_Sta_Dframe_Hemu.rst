Hemu
----------------------------------------





.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Sta.Dframe.Hemu.HemuCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.sta.dframe.hemu.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Sta_Dframe_Hemu_AlsField.rst
	Configure_Connection_Sta_Dframe_Hemu_BlAllocation.rst
	Configure_Connection_Sta_Dframe_Hemu_Dummy.rst
	Configure_Connection_Sta_Dframe_Hemu_RuAllocation.rst
	Configure_Connection_Sta_Dframe_Hemu_User.rst