Enums
=========

AccessCategory
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AccessCategory.ACBE
	# All values (4x):
	ACBE | ACBK | ACVI | ACVO

AccessNetType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AccessNetType.CPNetwork
	# All values (8x):
	CPNetwork | ESONetwork | FPNetwork | PDNetwork | PNETwork | PNWGaccess | TOEXperiment | WILDcard

AckType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AckType.ACK
	# All values (1x):
	ACK

AllocSize
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AllocSize.T106
	# All values (7x):
	T106 | T242 | T26 | T2X9 | T484 | T52 | T996

AuthAlgorithm
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AuthAlgorithm.MILenage
	# All values (2x):
	MILenage | XOR

AuthMethod
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AuthMethod.DISPlay
	# All values (3x):
	DISPlay | KEYPad | PBUTton

AuthType
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.AuthType.AKA
	# Last value:
	value = enums.AuthType.TTLS
	# All values (12x):
	AKA | AKAPrime | CLEap | GTC | IDENtity | MD5 | NAK | NOTification
	OTP | SIM | TLS | TTLS

AutoManualMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.AutoManualMode.AUTO
	# All values (2x):
	AUTO | MANual

BarMethod
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.BarMethod.EXPBar
	# All values (3x):
	EXPBar | IMPBar | MUBar

BurstType
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.BurstType.ABURsts
	# Last value:
	value = enums.BurstType.VHTBursts
	# All values (9x):
	ABURsts | DCBursts | HESBursts | HTBursts | NHTBursts | OBURsts | OFF | ON
	VHTBursts

Ch20Index
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Ch20Index.CHA1
	# All values (4x):
	CHA1 | CHA2 | CHA3 | CHA4

Channel80MhZ
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Channel80MhZ.PRIMary
	# All values (2x):
	PRIMary | SECondary

ChannelBandwidth
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ChannelBandwidth.BW16
	# All values (5x):
	BW16 | BW20 | BW40 | BW80 | BW88

ChannelBandwidthDut
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ChannelBandwidthDut.BW160
	# All values (4x):
	BW160 | BW20 | BW40 | BW80

Coderate
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Coderate.BR12
	# Last value:
	value = enums.Coderate.QR34
	# All values (28x):
	BR12 | BR34 | C11Mbits | C55Mbits | D1MBit | D2MBits | MCS | MCS1
	MCS10 | MCS11 | MCS12 | MCS13 | MCS14 | MCS15 | MCS2 | MCS3
	MCS4 | MCS5 | MCS6 | MCS7 | MCS8 | MCS9 | Q1M12 | Q1M34
	Q6M23 | Q6M34 | QR12 | QR34

CodingType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.CodingType.BCC
	# All values (2x):
	BCC | LDPC

ConnectionAllowed
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ConnectionAllowed.ANY
	# All values (2x):
	ANY | SSID

ConnectionMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ConnectionMode.ACONnect
	# All values (2x):
	ACONnect | MANual

DataFormatExt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DataFormatExt.HEES
	# All values (7x):
	HEES | HEM | HES | HTG | HTM | NHT | VHT

DataRate
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.DataRate.MB1
	# Last value:
	value = enums.DataRate.MCS9
	# All values (28x):
	MB1 | MB11 | MB12 | MB18 | MB2 | MB24 | MB36 | MB48
	MB5 | MB54 | MB6 | MB9 | MCS0 | MCS1 | MCS10 | MCS11
	MCS12 | MCS13 | MCS14 | MCS15 | MCS2 | MCS3 | MCS4 | MCS5
	MCS6 | MCS7 | MCS8 | MCS9

DelayType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DelayType.BURSt
	# All values (2x):
	BURSt | CONStant

DeviceClass
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DeviceClass.A
	# All values (2x):
	A | B

DynFragment
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.DynFragment.L1
	# All values (4x):
	L1 | L2 | L3 | NO

EapType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EapType.AKA
	# All values (5x):
	AKA | APRime | SIM | TLS | TTLS

EnableState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EnableState.DISable
	# All values (2x):
	DISable | ENABle

EncryptionType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EncryptionType.AES
	# All values (4x):
	AES | DISabled | GCMP | TKIP

EntityOperationMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.EntityOperationMode.AP
	# All values (6x):
	AP | HSPot2 | IBSS | STATion | TESTmode | WDIRect

FilsProbe
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FilsProbe.FILS
	# All values (3x):
	FILS | OFF | PROBe

FlowType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FlowType.ANNounced
	# All values (2x):
	ANNounced | UNANnounced

FrameFormat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FrameFormat.HEMU
	# All values (6x):
	HEMU | HESU | HETB | HT | NHT | VHT

FrequencyBand
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.FrequencyBand.B6GHz
	# All values (2x):
	B6GHz | BS6Ghz

Giltf
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Giltf.L116
	# All values (3x):
	L116 | L216 | L432

GroupTransform
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GroupTransform.ECP256
	# All values (2x):
	ECP256 | ECP384

GuardInterval
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.GuardInterval.GI08
	# All values (5x):
	GI08 | GI16 | GI32 | LONG | SHORt

HashMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HashMode.BOTH
	# All values (3x):
	BOTH | H2E | HUNT

HeTbMainMeasState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.HeTbMainMeasState.OFF
	# All values (3x):
	OFF | RDY | RUN

IpAddrIndex
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IpAddrIndex.IP1
	# All values (3x):
	IP1 | IP2 | IP3

IpV6AddField
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IpV6AddField.AATNknown
	# All values (3x):
	AATNknown | ATAVailable | ATNavailable

IpV6AddFieldExt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IpV6AddFieldExt.AATNknown
	# All values (8x):
	AATNknown | ATNavailable | DNPiaavailab | PDNiaavailab | PIAavailable | PRIaavailabl | PSNiaavailab | SNPiaavailab

IpVersion
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IpVersion.IV4
	# All values (2x):
	IV4 | IV6

IpVersionExt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.IpVersionExt.IV4
	# All values (3x):
	IV4 | IV4V6 | IV6

KeyMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.KeyMode.FIXed
	# All values (2x):
	FIXed | RANDom

LenMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LenMode.DEFault
	# All values (4x):
	DEFault | OFF | ON | UDEFined

Level
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Level.LEV0
	# All values (4x):
	LEV0 | LEV1 | LEV2 | LEV3

LogCategoryB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LogCategoryB.EMPTy
	# All values (4x):
	EMPTy | ERRor | INFO | WARNing

LtfGi
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LtfGi.L208
	# All values (3x):
	L208 | L216 | L432

LtfType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.LtfType.X1
	# All values (3x):
	X1 | X2 | X4

McsIndex
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.McsIndex.MCS
	# Last value:
	value = enums.McsIndex.MCS9
	# All values (12x):
	MCS | MCS1 | MCS10 | MCS11 | MCS2 | MCS3 | MCS4 | MCS5
	MCS6 | MCS7 | MCS8 | MCS9

McsSupport
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.McsSupport.NOTSupported
	# All values (2x):
	NOTSupported | SUPPorted

MimoMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MimoMode.SMULtiplexin
	# All values (3x):
	SMULtiplexin | STBC | TXDiversity

MuMimoLongTrainField
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.MuMimoLongTrainField.MASK
	# All values (2x):
	MASK | SING

NdpSoundingMethod
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NdpSoundingMethod.NONTrigger
	# All values (2x):
	NONTrigger | TBASed

NdpSoundingType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NdpSoundingType.CQI
	# All values (3x):
	CQI | MU | SU

NetAuthTypeInd
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NetAuthTypeInd.ATConditions
	# All values (4x):
	ATConditions | DREDirection | HREDirection | OESupported

Ngrouping
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Ngrouping.GRP16
	# All values (2x):
	GRP16 | GRP4

NumColumns
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NumColumns.COL1
	# All values (2x):
	COL1 | COL2

NumOfDigits
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.NumOfDigits.THDigits
	# All values (2x):
	THDigits | TWDigits

Pattern
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Pattern.AONE
	# Last value:
	value = enums.Pattern.PT10
	# All values (37x):
	AONE | AZERo | PN1 | PN10 | PN11 | PN12 | PN13 | PN14
	PN15 | PN16 | PN17 | PN18 | PN19 | PN2 | PN20 | PN21
	PN22 | PN23 | PN24 | PN25 | PN26 | PN27 | PN28 | PN29
	PN3 | PN30 | PN31 | PN32 | PN4 | PN5 | PN6 | PN7
	PN8 | PN9 | PRANdom | PT01 | PT10

PayloadType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PayloadType.AONes
	# All values (6x):
	AONes | AZERoes | BP01 | BP10 | DEFault | PRANdom

PccBasebandBoard
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PccBasebandBoard.BBR1
	# Last value:
	value = enums.PccBasebandBoard.SUW44
	# All values (140x):
	BBR1 | BBR11 | BBR12 | BBR13 | BBR14 | BBR2 | BBR21 | BBR22
	BBR23 | BBR24 | BBR3 | BBR31 | BBR32 | BBR33 | BBR34 | BBR4
	BBR41 | BBR42 | BBR43 | BBR44 | BBT1 | BBT11 | BBT12 | BBT13
	BBT14 | BBT2 | BBT21 | BBT22 | BBT23 | BBT24 | BBT3 | BBT31
	BBT32 | BBT33 | BBT34 | BBT4 | BBT41 | BBT42 | BBT43 | BBT44
	SUA012 | SUA034 | SUA056 | SUA078 | SUA1 | SUA11 | SUA112 | SUA12
	SUA13 | SUA134 | SUA14 | SUA15 | SUA156 | SUA16 | SUA17 | SUA178
	SUA18 | SUA2 | SUA21 | SUA212 | SUA22 | SUA23 | SUA234 | SUA24
	SUA25 | SUA256 | SUA26 | SUA27 | SUA278 | SUA28 | SUA3 | SUA31
	SUA312 | SUA32 | SUA33 | SUA334 | SUA34 | SUA35 | SUA356 | SUA36
	SUA37 | SUA378 | SUA38 | SUA4 | SUA41 | SUA412 | SUA42 | SUA43
	SUA434 | SUA44 | SUA45 | SUA456 | SUA46 | SUA47 | SUA478 | SUA48
	SUA5 | SUA6 | SUA7 | SUA8 | SUU1 | SUU11 | SUU12 | SUU13
	SUU14 | SUU2 | SUU21 | SUU22 | SUU23 | SUU24 | SUU3 | SUU31
	SUU32 | SUU33 | SUU34 | SUU4 | SUU41 | SUU42 | SUU43 | SUU44
	SUW1 | SUW11 | SUW12 | SUW13 | SUW14 | SUW2 | SUW21 | SUW22
	SUW23 | SUW24 | SUW3 | SUW31 | SUW32 | SUW33 | SUW34 | SUW4
	SUW41 | SUW42 | SUW43 | SUW44

PccFadingBoard
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.PccFadingBoard.FAD012
	# Last value:
	value = enums.PccFadingBoard.FAD8
	# All values (60x):
	FAD012 | FAD034 | FAD056 | FAD078 | FAD1 | FAD11 | FAD112 | FAD12
	FAD13 | FAD134 | FAD14 | FAD15 | FAD156 | FAD16 | FAD17 | FAD178
	FAD18 | FAD2 | FAD21 | FAD212 | FAD22 | FAD23 | FAD234 | FAD24
	FAD25 | FAD256 | FAD26 | FAD27 | FAD278 | FAD28 | FAD3 | FAD31
	FAD312 | FAD32 | FAD33 | FAD334 | FAD34 | FAD35 | FAD356 | FAD36
	FAD37 | FAD378 | FAD38 | FAD4 | FAD41 | FAD412 | FAD42 | FAD43
	FAD434 | FAD44 | FAD45 | FAD456 | FAD46 | FAD47 | FAD478 | FAD48
	FAD5 | FAD6 | FAD7 | FAD8

PeDuration
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PeDuration.AUTO
	# All values (6x):
	AUTO | PE0 | PE12 | PE16 | PE4 | PE8

PowerIndicator
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PowerIndicator.OVERdriven
	# All values (3x):
	OVERdriven | RANGe | UNDerdriven

PrioMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PrioMode.AUTO
	# All values (3x):
	AUTO | ROURobin | TIDPriority

PrioModeB
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PrioModeB.AUTO
	# All values (2x):
	AUTO | ROURobin

Profile
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Profile.MODA
	# All values (6x):
	MODA | MODB | MODC | MODD | MODE | MODF

Protection
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Protection.REQuired
	# All values (3x):
	REQuired | SUPPorted | UNSupported

ProtocolType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ProtocolType.ICMP
	# All values (2x):
	ICMP | UDP

PsState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PsState.ASSociated
	# All values (7x):
	ASSociated | AUTHenticated | CTIMeout | DEAuthenticated | DISassociated | IDLE | PROBed

PulseLengthMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.PulseLengthMode.BLENgth
	# All values (5x):
	BLENgth | DEFault | OFF | ON | UDEFined

RateSupport
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RateSupport.DISabled
	# All values (3x):
	DISabled | MANDatory | OPTional

Repeat
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Repeat.CONTinuous
	# All values (2x):
	CONTinuous | SINGleshot

Reservation
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Reservation.ANY
	# All values (3x):
	ANY | OFF | SET

ResourceState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResourceState.ACTive
	# All values (8x):
	ACTive | ADJusted | INValid | OFF | PENDing | QUEued | RDY | RUN

ResultState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.ResultState.FAILure
	# All values (3x):
	FAILure | IDLE | SUCCess

RuAlloc
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RuAlloc.DMY1
	# All values (5x):
	DMY1 | DMY2 | DMY3 | OFF | USR1

RuAllocation
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RuAllocation.OFF
	# Last value:
	value = enums.RuAllocation.RU9
	# All values (70x):
	OFF | RU0 | RU1 | RU10 | RU11 | RU12 | RU13 | RU14
	RU15 | RU16 | RU17 | RU18 | RU19 | RU2 | RU20 | RU21
	RU22 | RU23 | RU24 | RU25 | RU26 | RU27 | RU28 | RU29
	RU3 | RU30 | RU31 | RU32 | RU33 | RU34 | RU35 | RU36
	RU37 | RU38 | RU39 | RU4 | RU40 | RU41 | RU42 | RU43
	RU44 | RU45 | RU46 | RU47 | RU48 | RU49 | RU5 | RU50
	RU51 | RU52 | RU53 | RU54 | RU55 | RU56 | RU57 | RU58
	RU59 | RU6 | RU60 | RU61 | RU62 | RU63 | RU64 | RU65
	RU66 | RU67 | RU68 | RU7 | RU8 | RU9

RuIndex
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RuIndex.RU1
	# Last value:
	value = enums.RuIndex.RU9
	# All values (9x):
	RU1 | RU2 | RU3 | RU4 | RU5 | RU6 | RU7 | RU8
	RU9

RuSize
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.RuSize.T106
	# All values (6x):
	T106 | T242 | T26 | T484 | T52 | T996

RxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConnector.I11I
	# Last value:
	value = enums.RxConnector.RH8
	# All values (163x):
	I11I | I13I | I15I | I17I | I21I | I23I | I25I | I27I
	I31I | I33I | I35I | I37I | I41I | I43I | I45I | I47I
	IFI1 | IFI2 | IFI3 | IFI4 | IFI5 | IFI6 | IQ1I | IQ3I
	IQ5I | IQ7I | R10D | R11 | R11C | R11D | R12 | R12C
	R12D | R12I | R13 | R13C | R14 | R14C | R14I | R15
	R16 | R17 | R18 | R21 | R21C | R22 | R22C | R22I
	R23 | R23C | R24 | R24C | R24I | R25 | R26 | R27
	R28 | R31 | R31C | R32 | R32C | R32I | R33 | R33C
	R34 | R34C | R34I | R35 | R36 | R37 | R38 | R41
	R41C | R42 | R42C | R42I | R43 | R43C | R44 | R44C
	R44I | R45 | R46 | R47 | R48 | RA1 | RA2 | RA3
	RA4 | RA5 | RA6 | RA7 | RA8 | RB1 | RB2 | RB3
	RB4 | RB5 | RB6 | RB7 | RB8 | RC1 | RC2 | RC3
	RC4 | RC5 | RC6 | RC7 | RC8 | RD1 | RD2 | RD3
	RD4 | RD5 | RD6 | RD7 | RD8 | RE1 | RE2 | RE3
	RE4 | RE5 | RE6 | RE7 | RE8 | RF1 | RF1C | RF2
	RF2C | RF2I | RF3 | RF3C | RF4 | RF4C | RF4I | RF5
	RF5C | RF6 | RF6C | RF7 | RF7C | RF8 | RF8C | RF9C
	RFAC | RFBC | RFBI | RG1 | RG2 | RG3 | RG4 | RG5
	RG6 | RG7 | RG8 | RH1 | RH2 | RH3 | RH4 | RH5
	RH6 | RH7 | RH8

RxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.RxConverter.IRX1
	# Last value:
	value = enums.RxConverter.RX44
	# All values (40x):
	IRX1 | IRX11 | IRX12 | IRX13 | IRX14 | IRX2 | IRX21 | IRX22
	IRX23 | IRX24 | IRX3 | IRX31 | IRX32 | IRX33 | IRX34 | IRX4
	IRX41 | IRX42 | IRX43 | IRX44 | RX1 | RX11 | RX12 | RX13
	RX14 | RX2 | RX21 | RX22 | RX23 | RX24 | RX3 | RX31
	RX32 | RX33 | RX34 | RX4 | RX41 | RX42 | RX43 | RX44

Scenario
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Scenario.MIMFading
	# All values (6x):
	MIMFading | MIMO | MIMO2 | SCFading | STANdard | UNDefined

SecurityType
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.SecurityType.AUTO
	# Last value:
	value = enums.SecurityType.WPERsonal
	# All values (9x):
	AUTO | DISabled | OWE | W2ENterprise | W2Personal | W3ENterprise | W3Personal | WENTerprise
	WPERsonal

SegmentNumber
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SegmentNumber.A
	# All values (3x):
	A | B | C

Size
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Size.SIZE0
	# All values (2x):
	SIZE0 | SIZE1

SmoothingBit
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SmoothingBit.NRECommended
	# All values (2x):
	NRECommended | RECommended

SourceInt
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SourceInt.EXTernal
	# All values (2x):
	EXTernal | INTernal

SpacialStreamsNr
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SpacialStreamsNr.NSS1
	# All values (8x):
	NSS1 | NSS2 | NSS3 | NSS4 | NSS5 | NSS6 | NSS7 | NSS8

SpatialStreams
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SpatialStreams.ALL
	# All values (5x):
	ALL | OFF | ON | STR1 | STR2

StandardType
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.StandardType.ACSTd
	# Last value:
	value = enums.StandardType.NGFStd
	# All values (10x):
	ACSTd | ANSTd | ASTD | AXSTd | BSTD | GNSTd | GONStd | GOSTd
	GSTD | NGFStd

Station
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Station.STA1
	# All values (3x):
	STA1 | STA2 | STA3

Streams
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Streams.STR1
	# All values (2x):
	STR1 | STR2

Subfield
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.Subfield.A000
	# Last value:
	value = enums.Subfield.A224
	# All values (39x):
	A000 | A001 | A002 | A003 | A004 | A005 | A006 | A007
	A008 | A009 | A010 | A011 | A012 | A013 | A014 | A015
	A016 | A024 | A032 | A040 | A048 | A056 | A064 | A072
	A080 | A088 | A096 | A112 | A113 | A114 | A115 | A116
	A120 | A128 | A192 | A200 | A208 | A216 | A224

SyncState
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.SyncState.ADINtermed
	# All values (7x):
	ADINtermed | ADJusted | INValid | OFF | ON | PENDing | RFHandover

Tid
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.Tid.TID0
	# All values (8x):
	TID0 | TID1 | TID2 | TID3 | TID4 | TID5 | TID6 | TID7

TpControl
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TpControl.INDoor
	# All values (5x):
	INDoor | INENabled | INSTdpower | STANdard | VERYlowpow

TriggerBandwidth
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerBandwidth.ALL
	# All values (7x):
	ALL | BW160 | BW20 | BW40 | BW80 | OFF | ON

TriggerFrmPowerMode
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerFrmPowerMode.AUTO
	# All values (3x):
	AUTO | MANual | MAXPower

TriggerRate
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TriggerRate.ALL
	# Last value:
	value = enums.TriggerRate.QR34
	# All values (31x):
	ALL | BR12 | BR34 | C11Mbits | C55Mbits | D1MBit | D2MBits | MCS0
	MCS1 | MCS10 | MCS11 | MCS12 | MCS13 | MCS14 | MCS15 | MCS2
	MCS3 | MCS4 | MCS5 | MCS6 | MCS7 | MCS8 | MCS9 | OFF
	ON | Q1M12 | Q1M34 | Q6M23 | Q6M34 | QR12 | QR34

TriggerSlope
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerSlope.FEDGe
	# All values (4x):
	FEDGe | OFF | ON | REDGe

TriggerType
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.TriggerType.BQRP
	# All values (5x):
	BQRP | BRP | BSRP | BTR | MRTS

TxConnector
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TxConnector.I12O
	# Last value:
	value = enums.TxConnector.RH18
	# All values (86x):
	I12O | I14O | I16O | I18O | I22O | I24O | I26O | I28O
	I32O | I34O | I36O | I38O | I42O | I44O | I46O | I48O
	IFO1 | IFO2 | IFO3 | IFO4 | IFO5 | IFO6 | IQ2O | IQ4O
	IQ6O | IQ8O | R10D | R118 | R1183 | R1184 | R11C | R11D
	R11O | R11O3 | R11O4 | R12C | R12D | R13C | R13O | R14C
	R214 | R218 | R21C | R21O | R22C | R23C | R23O | R24C
	R258 | R318 | R31C | R31O | R32C | R33C | R33O | R34C
	R418 | R41C | R41O | R42C | R43C | R43O | R44C | RA18
	RB14 | RB18 | RC18 | RD18 | RE18 | RF18 | RF1C | RF1O
	RF2C | RF3C | RF3O | RF4C | RF5C | RF6C | RF7C | RF8C
	RF9C | RFAC | RFAO | RFBC | RG18 | RH18

TxConverter
----------------------------------------------------

.. code-block:: python

	# First value:
	value = enums.TxConverter.ITX1
	# Last value:
	value = enums.TxConverter.TX44
	# All values (40x):
	ITX1 | ITX11 | ITX12 | ITX13 | ITX14 | ITX2 | ITX21 | ITX22
	ITX23 | ITX24 | ITX3 | ITX31 | ITX32 | ITX33 | ITX34 | ITX4
	ITX41 | ITX42 | ITX43 | ITX44 | TX1 | TX11 | TX12 | TX13
	TX14 | TX2 | TX21 | TX22 | TX23 | TX24 | TX3 | TX31
	TX32 | TX33 | TX34 | TX4 | TX41 | TX42 | TX43 | TX44

VhtRates
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.VhtRates.MC07
	# All values (3x):
	MC07 | MC08 | MC09

YesNoStatus
----------------------------------------------------

.. code-block:: python

	# Example value:
	value = enums.YesNoStatus.NO
	# All values (2x):
	NO | YES

