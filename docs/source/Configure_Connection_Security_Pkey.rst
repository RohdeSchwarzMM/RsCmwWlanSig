Pkey
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:SECurity:PKEY

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:SECurity:PKEY



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Security.Pkey.PkeyCls
	:members:
	:undoc-members:
	:noindex: