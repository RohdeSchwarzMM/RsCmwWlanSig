Wdconf
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:WDIRect:WDConf

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:WDIRect:WDConf



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Wdirect.Wdconf.WdconfCls
	:members:
	:undoc-members:
	:noindex: