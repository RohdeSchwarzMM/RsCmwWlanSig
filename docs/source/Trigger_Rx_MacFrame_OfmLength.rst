OfmLength
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:WLAN:SIGNaling<instance>:RX:MACFrame:OFMLength

.. code-block:: python

	TRIGger:WLAN:SIGNaling<instance>:RX:MACFrame:OFMLength



.. autoclass:: RsCmwWlanSig.Implementations.Trigger.Rx.MacFrame.OfmLength.OfmLengthCls
	:members:
	:undoc-members:
	:noindex: