Per
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:PER:FDEF
	single: CONFigure:WLAN:SIGNaling<instance>:PER:DPATtern
	single: CONFigure:WLAN:SIGNaling<instance>:PER:DINTerval
	single: CONFigure:WLAN:SIGNaling<instance>:PER:TIDentifier
	single: CONFigure:WLAN:SIGNaling<instance>:PER:DESTination
	single: CONFigure:WLAN:SIGNaling<instance>:PER:PACKets
	single: CONFigure:WLAN:SIGNaling<instance>:PER:ATYPe
	single: CONFigure:WLAN:SIGNaling<instance>:PER:REPetition

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:PER:FDEF
	CONFigure:WLAN:SIGNaling<instance>:PER:DPATtern
	CONFigure:WLAN:SIGNaling<instance>:PER:DINTerval
	CONFigure:WLAN:SIGNaling<instance>:PER:TIDentifier
	CONFigure:WLAN:SIGNaling<instance>:PER:DESTination
	CONFigure:WLAN:SIGNaling<instance>:PER:PACKets
	CONFigure:WLAN:SIGNaling<instance>:PER:ATYPe
	CONFigure:WLAN:SIGNaling<instance>:PER:REPetition



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Per.PerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.per.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Per_Dframe.rst
	Configure_Per_Payload.rst