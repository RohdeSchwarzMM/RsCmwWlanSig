PgStats
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:WLAN:SIGNaling<instance>:PGEN<index>:PGSTats

.. code-block:: python

	SENSe:WLAN:SIGNaling<instance>:PGEN<index>:PGSTats



.. autoclass:: RsCmwWlanSig.Implementations.Sense.Pgen.PgStats.PgStatsCls
	:members:
	:undoc-members:
	:noindex: