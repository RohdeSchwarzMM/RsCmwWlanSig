Per
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:WLAN:SIGNaling<instance>:PER
	single: FETCh:WLAN:SIGNaling<instance>:PER
	single: STOP:WLAN:SIGNaling<instance>:PER
	single: ABORt:WLAN:SIGNaling<instance>:PER
	single: INITiate:WLAN:SIGNaling<instance>:PER

.. code-block:: python

	READ:WLAN:SIGNaling<instance>:PER
	FETCh:WLAN:SIGNaling<instance>:PER
	STOP:WLAN:SIGNaling<instance>:PER
	ABORt:WLAN:SIGNaling<instance>:PER
	INITiate:WLAN:SIGNaling<instance>:PER



.. autoclass:: RsCmwWlanSig.Implementations.Per.PerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.per.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Per_State.rst