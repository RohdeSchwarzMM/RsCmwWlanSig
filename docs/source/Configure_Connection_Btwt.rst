Btwt
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:BTWT:ENABle

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:BTWT:ENABle



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Btwt.BtwtCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.btwt.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Btwt_Schedule.rst