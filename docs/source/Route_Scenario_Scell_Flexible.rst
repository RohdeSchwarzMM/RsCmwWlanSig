Flexible
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:WLAN:SIGNaling<instance>:SCENario:SCELl:FLEXible

.. code-block:: python

	ROUTe:WLAN:SIGNaling<instance>:SCENario:SCELl:FLEXible



.. autoclass:: RsCmwWlanSig.Implementations.Route.Scenario.Scell.Flexible.FlexibleCls
	:members:
	:undoc-members:
	:noindex: