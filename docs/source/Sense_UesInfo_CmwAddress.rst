CmwAddress<IpVersion>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: V4 .. V6
	rc = driver.sense.uesInfo.cmwAddress.repcap_ipVersion_get()
	driver.sense.uesInfo.cmwAddress.repcap_ipVersion_set(repcap.IpVersion.V4)





.. autoclass:: RsCmwWlanSig.Implementations.Sense.UesInfo.CmwAddress.CmwAddressCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.uesInfo.cmwAddress.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UesInfo_CmwAddress_Ipv.rst