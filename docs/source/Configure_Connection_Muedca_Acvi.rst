Acvi
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:MUEDca:ACVI

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:MUEDca:ACVI



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Muedca.Acvi.AcviCls
	:members:
	:undoc-members:
	:noindex: