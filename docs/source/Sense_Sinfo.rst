Sinfo
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:WLAN:SIGNaling<instance>:SINFo:EAPStat

.. code-block:: python

	SENSe:WLAN:SIGNaling<instance>:SINFo:EAPStat



.. autoclass:: RsCmwWlanSig.Implementations.Sense.Sinfo.SinfoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.sinfo.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Sinfo_Antenna.rst