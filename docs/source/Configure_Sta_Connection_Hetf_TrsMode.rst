TrsMode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:STA<s>:CONNection:HETF:TRSMode

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:STA<s>:CONNection:HETF:TRSMode



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Sta.Connection.Hetf.TrsMode.TrsModeCls
	:members:
	:undoc-members:
	:noindex: