Connection
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:IVSupport
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:OMODe
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:MSTation
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:SMOothing
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:PAINterrupt
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:SYNC
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:SSID
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:BSSColor
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:BSSid
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:BEACon
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:DPERiod
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:STANdard
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:DSSS

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:IVSupport
	CONFigure:WLAN:SIGNaling<instance>:CONNection:OMODe
	CONFigure:WLAN:SIGNaling<instance>:CONNection:MSTation
	CONFigure:WLAN:SIGNaling<instance>:CONNection:SMOothing
	CONFigure:WLAN:SIGNaling<instance>:CONNection:PAINterrupt
	CONFigure:WLAN:SIGNaling<instance>:CONNection:SYNC
	CONFigure:WLAN:SIGNaling<instance>:CONNection:SSID
	CONFigure:WLAN:SIGNaling<instance>:CONNection:BSSColor
	CONFigure:WLAN:SIGNaling<instance>:CONNection:BSSid
	CONFigure:WLAN:SIGNaling<instance>:CONNection:BEACon
	CONFigure:WLAN:SIGNaling<instance>:CONNection:DPERiod
	CONFigure:WLAN:SIGNaling<instance>:CONNection:STANdard
	CONFigure:WLAN:SIGNaling<instance>:CONNection:DSSS



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.ConnectionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Aid.rst
	Configure_Connection_Association.rst
	Configure_Connection_Btwt.rst
	Configure_Connection_Ccode.rst
	Configure_Connection_DyFragment.rst
	Configure_Connection_Edca.rst
	Configure_Connection_Hemac.rst
	Configure_Connection_Hetf.rst
	Configure_Connection_Hotspot.rst
	Configure_Connection_MfDef.rst
	Configure_Connection_Muedca.rst
	Configure_Connection_NdpSounding.rst
	Configure_Connection_OobDiscovery.rst
	Configure_Connection_Qos.rst
	Configure_Connection_Security.rst
	Configure_Connection_Srates.rst
	Configure_Connection_Sta.rst
	Configure_Connection_Station.rst
	Configure_Connection_TpControl.rst
	Configure_Connection_Twt.rst
	Configure_Connection_Wdirect.rst