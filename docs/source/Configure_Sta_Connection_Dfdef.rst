Dfdef
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:STA<s>:CONNection:DFDef

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:STA<s>:CONNection:DFDef



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Sta.Connection.Dfdef.DfdefCls
	:members:
	:undoc-members:
	:noindex: