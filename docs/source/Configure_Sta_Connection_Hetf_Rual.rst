Rual
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:STA<s>:CONNection:HETF:RUAL

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:STA<s>:CONNection:HETF:RUAL



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Sta.Connection.Hetf.Rual.RualCls
	:members:
	:undoc-members:
	:noindex: