DsssConf
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:SRATes:DSSSconf

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:SRATes:DSSSconf



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Srates.DsssConf.DsssConfCls
	:members:
	:undoc-members:
	:noindex: