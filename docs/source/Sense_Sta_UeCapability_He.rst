He
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:WLAN:SIGNaling<instance>:STA<s>:UECapability:HE

.. code-block:: python

	SENSe:WLAN:SIGNaling<instance>:STA<s>:UECapability:HE



.. autoclass:: RsCmwWlanSig.Implementations.Sense.Sta.UeCapability.He.HeCls
	:members:
	:undoc-members:
	:noindex: