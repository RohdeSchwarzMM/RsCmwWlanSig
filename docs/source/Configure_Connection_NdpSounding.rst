NdpSounding
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:METHod
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:TARGet
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:TYPE
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:BW
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:SPSTreams
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:LTFGi
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:RUSTart
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:RUENd
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:CBOok
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:NUMColumns
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:SUBGrouping
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:TXP
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:TXEN

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:METHod
	CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:TARGet
	CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:TYPE
	CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:BW
	CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:SPSTreams
	CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:LTFGi
	CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:RUSTart
	CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:RUENd
	CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:CBOok
	CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:NUMColumns
	CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:SUBGrouping
	CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:TXP
	CONFigure:WLAN:SIGNaling<instance>:CONNection:NDPSounding:TXEN



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.NdpSounding.NdpSoundingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.ndpSounding.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_NdpSounding_SsTx.rst