Mimo
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:MIMO:TMMode

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:MIMO:TMMode



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Mimo.MimoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.mimo.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Mimo_Tcsd.rst