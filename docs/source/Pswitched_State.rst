State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:SIGNaling<instance>:PSWitched:STATe

.. code-block:: python

	FETCh:WLAN:SIGNaling<instance>:PSWitched:STATe



.. autoclass:: RsCmwWlanSig.Implementations.Pswitched.State.StateCls
	:members:
	:undoc-members:
	:noindex: