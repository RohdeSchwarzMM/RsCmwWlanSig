Sta<Station>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.call.sta.repcap_station_get()
	driver.call.sta.repcap_station_set(repcap.Station.Nr1)





.. autoclass:: RsCmwWlanSig.Implementations.Call.Sta.StaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.call.sta.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Call_Sta_Action.rst