EpePower
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:RFSettings:ANTenna<n>:EPEPower

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:RFSettings:ANTenna<n>:EPEPower



.. autoclass:: RsCmwWlanSig.Implementations.Configure.RfSettings.Antenna.EpePower.EpePowerCls
	:members:
	:undoc-members:
	:noindex: