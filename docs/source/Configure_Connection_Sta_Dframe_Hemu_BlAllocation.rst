BlAllocation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:STA<s>:DFRame:HEMU:BLALlocation

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:STA<s>:DFRame:HEMU:BLALlocation



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Sta.Dframe.Hemu.BlAllocation.BlAllocationCls
	:members:
	:undoc-members:
	:noindex: