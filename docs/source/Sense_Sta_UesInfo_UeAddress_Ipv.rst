Ipv
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:WLAN:SIGNaling<instance>:STA<s>:UESinfo:UEADdress:IPV<n>

.. code-block:: python

	SENSe:WLAN:SIGNaling<instance>:STA<s>:UESinfo:UEADdress:IPV<n>



.. autoclass:: RsCmwWlanSig.Implementations.Sense.Sta.UesInfo.UeAddress.Ipv.IpvCls
	:members:
	:undoc-members:
	:noindex: