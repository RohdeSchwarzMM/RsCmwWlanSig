Aid
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:AID

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:AID



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Aid.AidCls
	:members:
	:undoc-members:
	:noindex: