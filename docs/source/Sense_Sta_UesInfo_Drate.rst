Drate
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:WLAN:SIGNaling<instance>:STA<s>:UESinfo:DRATe

.. code-block:: python

	SENSe:WLAN:SIGNaling<instance>:STA<s>:UESinfo:DRATe



.. autoclass:: RsCmwWlanSig.Implementations.Sense.Sta.UesInfo.Drate.DrateCls
	:members:
	:undoc-members:
	:noindex: