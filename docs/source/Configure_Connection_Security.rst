Security
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:SECurity:ENCRyption
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:SECurity:PMF
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:SECurity:GTRansform
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:SECurity:HASHtoelem

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:SECurity:ENCRyption
	CONFigure:WLAN:SIGNaling<instance>:CONNection:SECurity:PMF
	CONFigure:WLAN:SIGNaling<instance>:CONNection:SECurity:GTRansform
	CONFigure:WLAN:SIGNaling<instance>:CONNection:SECurity:HASHtoelem



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Security.SecurityCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.security.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Security_Eaka.rst
	Configure_Connection_Security_Esim.rst
	Configure_Connection_Security_Passphrase.rst
	Configure_Connection_Security_Pkey.rst
	Configure_Connection_Security_Rserver.rst
	Configure_Connection_Security_TypePy.rst