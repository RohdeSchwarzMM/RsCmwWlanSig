Scenario
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:WLAN:SIGNaling<instance>:SCENario

.. code-block:: python

	ROUTe:WLAN:SIGNaling<instance>:SCENario



.. autoclass:: RsCmwWlanSig.Implementations.Route.Scenario.ScenarioCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_MimFading.rst
	Route_Scenario_Mimo.rst
	Route_Scenario_Scell.rst
	Route_Scenario_ScFading.rst