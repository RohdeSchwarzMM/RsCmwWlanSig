Scell
----------------------------------------





.. autoclass:: RsCmwWlanSig.Implementations.Route.Scenario.Scell.ScellCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.scell.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_Scell_Flexible.rst