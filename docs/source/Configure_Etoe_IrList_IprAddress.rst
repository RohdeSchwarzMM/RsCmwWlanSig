IprAddress<IpRouteAddress>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr5
	rc = driver.configure.etoe.irList.iprAddress.repcap_ipRouteAddress_get()
	driver.configure.etoe.irList.iprAddress.repcap_ipRouteAddress_set(repcap.IpRouteAddress.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:ETOE:IRList:IPRaddress<n>

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:ETOE:IRList:IPRaddress<n>



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Etoe.IrList.IprAddress.IprAddressCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.etoe.irList.iprAddress.clone()