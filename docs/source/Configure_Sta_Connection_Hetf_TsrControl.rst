TsrControl
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:STA<s>:CONNection:HETF:TSRControl

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:STA<s>:CONNection:HETF:TSRControl



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Sta.Connection.Hetf.TsrControl.TsrControlCls
	:members:
	:undoc-members:
	:noindex: