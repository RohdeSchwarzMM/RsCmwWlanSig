Pgen<PacketGenerator>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr3
	rc = driver.sense.pgen.repcap_packetGenerator_get()
	driver.sense.pgen.repcap_packetGenerator_set(repcap.PacketGenerator.Nr1)





.. autoclass:: RsCmwWlanSig.Implementations.Sense.Pgen.PgenCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.pgen.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Pgen_PgStats.rst