Protocol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:PGEN<index>:PROTocol

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:PGEN<index>:PROTocol



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Pgen.Protocol.ProtocolCls
	:members:
	:undoc-members:
	:noindex: