Sconnection
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALL:WLAN:SIGNaling<Instance>:ACTion:WPS:SCONnection

.. code-block:: python

	CALL:WLAN:SIGNaling<Instance>:ACTion:WPS:SCONnection



.. autoclass:: RsCmwWlanSig.Implementations.Call.Action.Wps.Sconnection.SconnectionCls
	:members:
	:undoc-members:
	:noindex: