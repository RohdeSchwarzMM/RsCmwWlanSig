Dframe
----------------------------------------





.. autoclass:: RsCmwWlanSig.Implementations.Configure.Per.Dframe.DframeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.per.dframe.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Per_Dframe_Hemu.rst