MwDuration
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:BTWT:SCHedule:MWDuration

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:BTWT:SCHedule:MWDuration



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Btwt.Schedule.MwDuration.MwDurationCls
	:members:
	:undoc-members:
	:noindex: