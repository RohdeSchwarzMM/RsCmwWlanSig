Ctype
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:STA<s>:DFRame:HEMU:USER<index>:CTYPe

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:STA<s>:DFRame:HEMU:USER<index>:CTYPe



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Sta.Dframe.Hemu.User.Ctype.CtypeCls
	:members:
	:undoc-members:
	:noindex: