Plength
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:WLAN:SIGNaling<instance>:TX:MACFrame:PLENgth:MODE
	single: TRIGger:WLAN:SIGNaling<instance>:TX:MACFrame:PLENgth:VALue

.. code-block:: python

	TRIGger:WLAN:SIGNaling<instance>:TX:MACFrame:PLENgth:MODE
	TRIGger:WLAN:SIGNaling<instance>:TX:MACFrame:PLENgth:VALue



.. autoclass:: RsCmwWlanSig.Implementations.Trigger.Tx.MacFrame.Plength.PlengthCls
	:members:
	:undoc-members:
	:noindex: