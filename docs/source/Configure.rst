Configure
----------------------------------------





.. autoclass:: RsCmwWlanSig.Implementations.Configure.ConfigureCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection.rst
	Configure_Edau.rst
	Configure_Etoe.rst
	Configure_Fading.rst
	Configure_HetBased.rst
	Configure_IpvFour.rst
	Configure_IpvSix.rst
	Configure_Mimo.rst
	Configure_Mmonitor.rst
	Configure_Per.rst
	Configure_Pgen.rst
	Configure_RfSettings.rst
	Configure_Sta.rst
	Configure_UesInfo.rst