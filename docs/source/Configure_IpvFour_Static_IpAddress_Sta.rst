Sta
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:IPVFour:STATic:IPADdress:STA<s>

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:IPVFour:STATic:IPADdress:STA<s>



.. autoclass:: RsCmwWlanSig.Implementations.Configure.IpvFour.Static.IpAddress.Sta.StaCls
	:members:
	:undoc-members:
	:noindex: