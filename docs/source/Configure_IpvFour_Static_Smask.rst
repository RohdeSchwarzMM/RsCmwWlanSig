Smask
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:IPVFour:STATic:SMASk

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:IPVFour:STATic:SMASk



.. autoclass:: RsCmwWlanSig.Implementations.Configure.IpvFour.Static.Smask.SmaskCls
	:members:
	:undoc-members:
	:noindex: