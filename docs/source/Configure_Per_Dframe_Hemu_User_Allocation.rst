Allocation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:PER:DFRame:HEMU:USER<index>:ALLocation

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:PER:DFRame:HEMU:USER<index>:ALLocation



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Per.Dframe.Hemu.User.Allocation.AllocationCls
	:members:
	:undoc-members:
	:noindex: