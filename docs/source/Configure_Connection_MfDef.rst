MfDef
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:MFDef

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:MFDef



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.MfDef.MfDefCls
	:members:
	:undoc-members:
	:noindex: