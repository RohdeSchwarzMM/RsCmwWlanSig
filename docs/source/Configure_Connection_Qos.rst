Qos
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:QOS:ETOE
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:QOS:PRIoritiz

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:QOS:ETOE
	CONFigure:WLAN:SIGNaling<instance>:CONNection:QOS:PRIoritiz



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Qos.QosCls
	:members:
	:undoc-members:
	:noindex: