Antenna<Antenna>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.configure.rfSettings.antenna.repcap_antenna_get()
	driver.configure.rfSettings.antenna.repcap_antenna_set(repcap.Antenna.Nr1)





.. autoclass:: RsCmwWlanSig.Implementations.Configure.RfSettings.Antenna.AntennaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.antenna.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Antenna_Eattenuation.rst
	Configure_RfSettings_Antenna_EpePower.rst
	Configure_RfSettings_Antenna_MlOffset.rst