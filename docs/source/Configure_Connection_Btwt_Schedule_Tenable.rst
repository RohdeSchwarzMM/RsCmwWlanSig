Tenable
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:BTWT:SCHedule:TENable

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:BTWT:SCHedule:TENable



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Btwt.Schedule.Tenable.TenableCls
	:members:
	:undoc-members:
	:noindex: