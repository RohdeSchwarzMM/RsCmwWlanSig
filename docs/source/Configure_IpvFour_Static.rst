Static<Station>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.configure.ipvFour.static.repcap_station_get()
	driver.configure.ipvFour.static.repcap_station_set(repcap.Station.Nr1)





.. autoclass:: RsCmwWlanSig.Implementations.Configure.IpvFour.Static.StaticCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.ipvFour.static.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_IpvFour_Static_IpAddress.rst
	Configure_IpvFour_Static_Smask.rst