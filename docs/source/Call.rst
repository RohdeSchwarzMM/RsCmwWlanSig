Call
----------------------------------------





.. autoclass:: RsCmwWlanSig.Implementations.Call.CallCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.call.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Call_Action.rst
	Call_Sta.rst