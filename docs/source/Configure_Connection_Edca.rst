Edca
----------------------------------------





.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Edca.EdcaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.edca.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Edca_Acbe.rst
	Configure_Connection_Edca_Acbk.rst
	Configure_Connection_Edca_Acvi.rst
	Configure_Connection_Edca_Acvo.rst