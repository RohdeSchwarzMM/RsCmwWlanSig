Ccode
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:CCODe:CCSTate

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:CCODe:CCSTate



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Ccode.CcodeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.ccode.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Ccode_Ccconf.rst