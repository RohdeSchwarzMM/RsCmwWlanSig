Bandwidth
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:FADing:AWGN:BWIDth:RATio

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:FADing:AWGN:BWIDth:RATio



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Fading.Awgn.Bandwidth.BandwidthCls
	:members:
	:undoc-members:
	:noindex: