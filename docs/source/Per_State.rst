State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:SIGNaling<instance>:PER:STATe

.. code-block:: python

	FETCh:WLAN:SIGNaling<instance>:PER:STATe



.. autoclass:: RsCmwWlanSig.Implementations.Per.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.per.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Per_State_All.rst