Tcsd
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:MIMO:TCSD

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:MIMO:TCSD



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Mimo.Tcsd.TcsdCls
	:members:
	:undoc-members:
	:noindex: