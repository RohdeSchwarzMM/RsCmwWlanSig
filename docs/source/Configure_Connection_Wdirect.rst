Wdirect
----------------------------------------





.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Wdirect.WdirectCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.wdirect.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Wdirect_Atype.rst
	Configure_Connection_Wdirect_Wdconf.rst