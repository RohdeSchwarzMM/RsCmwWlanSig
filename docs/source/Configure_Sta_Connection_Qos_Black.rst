Black
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:STA<s>:CONNection:QOS:BLACk

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:STA<s>:CONNection:QOS:BLACk



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Sta.Connection.Qos.Black.BlackCls
	:members:
	:undoc-members:
	:noindex: