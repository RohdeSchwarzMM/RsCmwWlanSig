Action
----------------------------------------





.. autoclass:: RsCmwWlanSig.Implementations.Call.Sta.Action.ActionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.call.sta.action.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Call_Sta_Action_Disconnect.rst