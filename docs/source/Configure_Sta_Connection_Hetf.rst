Hetf
----------------------------------------





.. autoclass:: RsCmwWlanSig.Implementations.Configure.Sta.Connection.Hetf.HetfCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.sta.connection.hetf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Sta_Connection_Hetf_Ctyp.rst
	Configure_Sta_Connection_Hetf_Dcm.rst
	Configure_Sta_Connection_Hetf_Mcs.rst
	Configure_Sta_Connection_Hetf_Nss.rst
	Configure_Sta_Connection_Hetf_Rual.rst
	Configure_Sta_Connection_Hetf_Sss.rst
	Configure_Sta_Connection_Hetf_TrsMode.rst
	Configure_Sta_Connection_Hetf_Trssi.rst
	Configure_Sta_Connection_Hetf_TsrControl.rst