State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:SIGNaling<instance>:HETBased:STATe

.. code-block:: python

	FETCh:WLAN:SIGNaling<instance>:HETBased:STATe



.. autoclass:: RsCmwWlanSig.Implementations.HetBased.State.StateCls
	:members:
	:undoc-members:
	:noindex: