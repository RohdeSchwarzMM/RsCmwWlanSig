Kalgo
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:SECurity:EAKA:KALGo

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:SECurity:EAKA:KALGo



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Security.Eaka.Kalgo.KalgoCls
	:members:
	:undoc-members:
	:noindex: