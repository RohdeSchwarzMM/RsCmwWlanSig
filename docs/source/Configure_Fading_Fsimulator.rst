Fsimulator
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:FADing:FSIMulator:STANdard
	single: CONFigure:WLAN:SIGNaling<instance>:FADing:FSIMulator:ENABle

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:FADing:FSIMulator:STANdard
	CONFigure:WLAN:SIGNaling<instance>:FADing:FSIMulator:ENABle



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Fading.Fsimulator.FsimulatorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.fading.fsimulator.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Fading_Fsimulator_Iloss.rst