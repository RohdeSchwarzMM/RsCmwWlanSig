Dname<DomainName>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr5
	rc = driver.configure.connection.hotspot.dname.repcap_domainName_get()
	driver.configure.connection.hotspot.dname.repcap_domainName_set(repcap.DomainName.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:HOTSpot:DNAMe<nr>

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:HOTSpot:DNAMe<nr>



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Hotspot.Dname.DnameCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.hotspot.dname.clone()