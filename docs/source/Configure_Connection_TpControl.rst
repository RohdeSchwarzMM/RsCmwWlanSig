TpControl
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:TPControl:PWConstraint
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:TPControl:REGulatory

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:TPControl:PWConstraint
	CONFigure:WLAN:SIGNaling<instance>:CONNection:TPControl:REGulatory



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.TpControl.TpControlCls
	:members:
	:undoc-members:
	:noindex: