User<User>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr1
	rc = driver.configure.connection.sta.dframe.hemu.user.repcap_user_get()
	driver.configure.connection.sta.dframe.hemu.user.repcap_user_set(repcap.User.Nr1)





.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Sta.Dframe.Hemu.User.UserCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.sta.dframe.hemu.user.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Sta_Dframe_Hemu_User_Allocation.rst
	Configure_Connection_Sta_Dframe_Hemu_User_Ctype.rst
	Configure_Connection_Sta_Dframe_Hemu_User_Mcs.rst
	Configure_Connection_Sta_Dframe_Hemu_User_Streams.rst