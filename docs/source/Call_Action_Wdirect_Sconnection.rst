Sconnection
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CALL:WLAN:SIGNaling<Instance>:ACTion:WDIRect:SCONnection

.. code-block:: python

	CALL:WLAN:SIGNaling<Instance>:ACTion:WDIRect:SCONnection



.. autoclass:: RsCmwWlanSig.Implementations.Call.Action.Wdirect.Sconnection.SconnectionCls
	:members:
	:undoc-members:
	:noindex: