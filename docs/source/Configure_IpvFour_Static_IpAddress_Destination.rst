Destination
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:IPVFour:STATic:IPADdress:DESTination

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:IPVFour:STATic:IPADdress:DESTination



.. autoclass:: RsCmwWlanSig.Implementations.Configure.IpvFour.Static.IpAddress.Destination.DestinationCls
	:members:
	:undoc-members:
	:noindex: