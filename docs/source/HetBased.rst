HetBased
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:WLAN:SIGNaling<instance>:HETBased
	single: STOP:WLAN:SIGNaling<instance>:HETBased
	single: ABORt:WLAN:SIGNaling<instance>:HETBased

.. code-block:: python

	INITiate:WLAN:SIGNaling<instance>:HETBased
	STOP:WLAN:SIGNaling<instance>:HETBased
	ABORt:WLAN:SIGNaling<instance>:HETBased



.. autoclass:: RsCmwWlanSig.Implementations.HetBased.HetBasedCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.hetBased.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	HetBased_State.rst
	HetBased_UphInfo.rst