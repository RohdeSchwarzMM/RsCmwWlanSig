Payload
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:PER:PAYLoad:SIZE

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:PER:PAYLoad:SIZE



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Per.Payload.PayloadCls
	:members:
	:undoc-members:
	:noindex: