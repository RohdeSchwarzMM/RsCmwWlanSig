IpVersion
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:PGEN<index>:IPVersion

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:PGEN<index>:IPVersion



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Pgen.IpVersion.IpVersionCls
	:members:
	:undoc-members:
	:noindex: