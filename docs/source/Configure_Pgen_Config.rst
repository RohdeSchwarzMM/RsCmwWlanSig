Config
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:PGEN<index>:CONFig

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:PGEN<index>:CONFig



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Pgen.Config.ConfigCls
	:members:
	:undoc-members:
	:noindex: