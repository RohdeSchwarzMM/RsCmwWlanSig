RepCaps
=========

Instance (Global)
----------------------------------------------------

.. code-block:: python

	# Setting:
	driver.repcap_instance_set(repcap.Instance.Inst1)
	# Values (2x):
	Inst1 | Inst2

Antenna
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Antenna.Nr1
	# Values (2x):
	Nr1 | Nr2

DomainName
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.DomainName.Nr1
	# Range:
	Nr1 .. Nr5
	# All values (5x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5

Dummy
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Dummy.Nr1
	# Values (3x):
	Nr1 | Nr2 | Nr3

IpRouteAddress
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.IpRouteAddress.Nr1
	# Range:
	Nr1 .. Nr5
	# All values (5x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5

IpVersion
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.IpVersion.V4
	# Values (2x):
	V4 | V6

PacketGenerator
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.PacketGenerator.Nr1
	# Values (3x):
	Nr1 | Nr2 | Nr3

Plnm
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Plnm.Nr1
	# Range:
	Nr1 .. Nr5
	# All values (5x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5

Realm
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Realm.Nr1
	# Range:
	Nr1 .. Nr5
	# All values (5x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5

Station
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Station.Nr1
	# Values (4x):
	Nr1 | Nr2 | Nr3 | Nr4

User
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.User.Nr1
	# Values (1x):
	Nr1

