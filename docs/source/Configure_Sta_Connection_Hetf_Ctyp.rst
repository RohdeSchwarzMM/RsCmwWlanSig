Ctyp
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:STA<s>:CONNection:HETF:CTYP

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:STA<s>:CONNection:HETF:CTYP



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Sta.Connection.Hetf.Ctyp.CtypCls
	:members:
	:undoc-members:
	:noindex: