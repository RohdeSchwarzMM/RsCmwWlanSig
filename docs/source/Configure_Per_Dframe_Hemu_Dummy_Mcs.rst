Mcs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:PER:DFRame:HEMU:DUMMy<index>:MCS

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:PER:DFRame:HEMU:DUMMy<index>:MCS



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Per.Dframe.Hemu.Dummy.Mcs.McsCls
	:members:
	:undoc-members:
	:noindex: