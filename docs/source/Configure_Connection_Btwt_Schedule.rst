Schedule
----------------------------------------





.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Btwt.Schedule.ScheduleCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.btwt.schedule.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Btwt_Schedule_Enable.rst
	Configure_Connection_Btwt_Schedule_Ftype.rst
	Configure_Connection_Btwt_Schedule_MwDuration.rst
	Configure_Connection_Btwt_Schedule_Stime.rst
	Configure_Connection_Btwt_Schedule_Tenable.rst