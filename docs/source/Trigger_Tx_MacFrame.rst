MacFrame
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: TRIGger:WLAN:SIGNaling<instance>:TX:MACFrame:SLOPe

.. code-block:: python

	TRIGger:WLAN:SIGNaling<instance>:TX:MACFrame:SLOPe



.. autoclass:: RsCmwWlanSig.Implementations.Trigger.Tx.MacFrame.MacFrameCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.tx.macFrame.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Tx_MacFrame_Plength.rst