Passphrase
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:SECurity:PASSphrase

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:SECurity:PASSphrase



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Security.Passphrase.PassphraseCls
	:members:
	:undoc-members:
	:noindex: