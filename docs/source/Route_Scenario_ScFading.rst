ScFading
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:WLAN:SIGNaling<instance>:SCENario:SCFading:FLEXible

.. code-block:: python

	ROUTe:WLAN:SIGNaling<instance>:SCENario:SCFading:FLEXible



.. autoclass:: RsCmwWlanSig.Implementations.Route.Scenario.ScFading.ScFadingCls
	:members:
	:undoc-members:
	:noindex: