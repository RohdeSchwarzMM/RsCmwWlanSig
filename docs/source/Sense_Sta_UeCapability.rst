UeCapability
----------------------------------------





.. autoclass:: RsCmwWlanSig.Implementations.Sense.Sta.UeCapability.UeCapabilityCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.sta.ueCapability.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Sta_UeCapability_He.rst
	Sense_Sta_UeCapability_Mac.rst