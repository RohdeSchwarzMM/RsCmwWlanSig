Streams
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:PER:DFRame:HEMU:USER<index>:STReams

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:PER:DFRame:HEMU:USER<index>:STReams



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Per.Dframe.Hemu.User.Streams.StreamsCls
	:members:
	:undoc-members:
	:noindex: