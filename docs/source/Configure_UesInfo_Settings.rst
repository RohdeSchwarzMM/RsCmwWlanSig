Settings
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:UESinfo:SETTings

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:UESinfo:SETTings



.. autoclass:: RsCmwWlanSig.Implementations.Configure.UesInfo.Settings.SettingsCls
	:members:
	:undoc-members:
	:noindex: