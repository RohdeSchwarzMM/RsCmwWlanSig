Route
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:WLAN:SIGNaling<instance>

.. code-block:: python

	ROUTe:WLAN:SIGNaling<instance>



.. autoclass:: RsCmwWlanSig.Implementations.Route.RouteCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario.rst