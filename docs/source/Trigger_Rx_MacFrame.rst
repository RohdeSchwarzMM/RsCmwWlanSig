MacFrame
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:WLAN:SIGNaling<instance>:RX:MACFrame:BTYPe
	single: TRIGger:WLAN:SIGNaling<instance>:RX:MACFrame:BW
	single: TRIGger:WLAN:SIGNaling<instance>:RX:MACFrame:STReams
	single: TRIGger:WLAN:SIGNaling<instance>:RX:MACFrame:RATE
	single: TRIGger:WLAN:SIGNaling<instance>:RX:MACFrame:CTDelay
	single: TRIGger:WLAN:SIGNaling<instance>:RX:MACFrame:RREStriction
	single: TRIGger:WLAN:SIGNaling<instance>:RX:MACFrame:SLOPe

.. code-block:: python

	TRIGger:WLAN:SIGNaling<instance>:RX:MACFrame:BTYPe
	TRIGger:WLAN:SIGNaling<instance>:RX:MACFrame:BW
	TRIGger:WLAN:SIGNaling<instance>:RX:MACFrame:STReams
	TRIGger:WLAN:SIGNaling<instance>:RX:MACFrame:RATE
	TRIGger:WLAN:SIGNaling<instance>:RX:MACFrame:CTDelay
	TRIGger:WLAN:SIGNaling<instance>:RX:MACFrame:RREStriction
	TRIGger:WLAN:SIGNaling<instance>:RX:MACFrame:SLOPe



.. autoclass:: RsCmwWlanSig.Implementations.Trigger.Rx.MacFrame.MacFrameCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.rx.macFrame.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Rx_MacFrame_DsmLength.rst
	Trigger_Rx_MacFrame_OfmLength.rst
	Trigger_Rx_MacFrame_Plength.rst