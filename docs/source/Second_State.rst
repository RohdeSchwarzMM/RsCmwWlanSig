State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:WLAN:SIGNaling<instance>:SECond:STATe

.. code-block:: python

	FETCh:WLAN:SIGNaling<instance>:SECond:STATe



.. autoclass:: RsCmwWlanSig.Implementations.Second.State.StateCls
	:members:
	:undoc-members:
	:noindex: