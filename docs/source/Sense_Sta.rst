Sta<Station>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.sense.sta.repcap_station_get()
	driver.sense.sta.repcap_station_set(repcap.Station.Nr1)





.. autoclass:: RsCmwWlanSig.Implementations.Sense.Sta.StaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.sta.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_Sta_HetbInfo.rst
	Sense_Sta_UeCapability.rst
	Sense_Sta_UesInfo.rst