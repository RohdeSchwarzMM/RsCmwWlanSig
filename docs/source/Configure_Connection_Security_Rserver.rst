Rserver
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:SECurity:RSERver:MODE
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:SECurity:RSERver:SKEY
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:SECurity:RSERver:PNUMber

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:SECurity:RSERver:MODE
	CONFigure:WLAN:SIGNaling<instance>:CONNection:SECurity:RSERver:SKEY
	CONFigure:WLAN:SIGNaling<instance>:CONNection:SECurity:RSERver:PNUMber



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Security.Rserver.RserverCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.security.rserver.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Security_Rserver_Iconf.rst