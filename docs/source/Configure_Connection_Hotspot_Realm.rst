Realm<Realm>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr5
	rc = driver.configure.connection.hotspot.realm.repcap_realm_get()
	driver.configure.connection.hotspot.realm.repcap_realm_set(repcap.Realm.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:HOTSpot:REALm<nr>

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:HOTSpot:REALm<nr>



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Hotspot.Realm.RealmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.hotspot.realm.clone()