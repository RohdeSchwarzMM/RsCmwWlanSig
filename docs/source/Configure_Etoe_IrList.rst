IrList
----------------------------------------





.. autoclass:: RsCmwWlanSig.Implementations.Configure.Etoe.IrList.IrListCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.etoe.irList.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Etoe_IrList_IprAddress.rst