Gateway
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:IPVFour:STATic:IPADdress:GATeway

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:IPVFour:STATic:IPADdress:GATeway



.. autoclass:: RsCmwWlanSig.Implementations.Configure.IpvFour.Static.IpAddress.Gateway.GatewayCls
	:members:
	:undoc-members:
	:noindex: