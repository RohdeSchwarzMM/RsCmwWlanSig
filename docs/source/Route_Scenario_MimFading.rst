MimFading
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:WLAN:SIGNaling<instance>:SCENario:MIMFading:FLEXible

.. code-block:: python

	ROUTe:WLAN:SIGNaling<instance>:SCENario:MIMFading:FLEXible



.. autoclass:: RsCmwWlanSig.Implementations.Route.Scenario.MimFading.MimFadingCls
	:members:
	:undoc-members:
	:noindex: