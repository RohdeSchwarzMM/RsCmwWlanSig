Hemu
----------------------------------------





.. autoclass:: RsCmwWlanSig.Implementations.Configure.Per.Dframe.Hemu.HemuCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.per.dframe.hemu.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Per_Dframe_Hemu_AlsField.rst
	Configure_Per_Dframe_Hemu_BlAllocation.rst
	Configure_Per_Dframe_Hemu_Dummy.rst
	Configure_Per_Dframe_Hemu_RuAllocation.rst
	Configure_Per_Dframe_Hemu_User.rst