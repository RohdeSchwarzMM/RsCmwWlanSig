Stime
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:BTWT:SCHedule:STIMe

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:BTWT:SCHedule:STIMe



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Btwt.Schedule.Stime.StimeCls
	:members:
	:undoc-members:
	:noindex: