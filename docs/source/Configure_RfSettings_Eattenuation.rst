Eattenuation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:RFSettings:EATTenuation:INPut

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:RFSettings:EATTenuation:INPut



.. autoclass:: RsCmwWlanSig.Implementations.Configure.RfSettings.Eattenuation.EattenuationCls
	:members:
	:undoc-members:
	:noindex: