HetBased
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:HETBased:FRAMes

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:HETBased:FRAMes



.. autoclass:: RsCmwWlanSig.Implementations.Configure.HetBased.HetBasedCls
	:members:
	:undoc-members:
	:noindex: