Pgen<PacketGenerator>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr3
	rc = driver.configure.pgen.repcap_packetGenerator_get()
	driver.configure.pgen.repcap_packetGenerator_set(repcap.PacketGenerator.Nr1)





.. autoclass:: RsCmwWlanSig.Implementations.Configure.Pgen.PgenCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.pgen.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Pgen_Config.rst
	Configure_Pgen_Destination.rst
	Configure_Pgen_IpVersion.rst
	Configure_Pgen_Protocol.rst
	Configure_Pgen_Uports.rst