Ftype
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:BTWT:SCHedule:FTYPe

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:BTWT:SCHedule:FTYPe



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Btwt.Schedule.Ftype.FtypeCls
	:members:
	:undoc-members:
	:noindex: