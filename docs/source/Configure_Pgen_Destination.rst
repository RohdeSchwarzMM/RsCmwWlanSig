Destination
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:PGEN<index>:DESTination

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:PGEN<index>:DESTination



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Pgen.Destination.DestinationCls
	:members:
	:undoc-members:
	:noindex: