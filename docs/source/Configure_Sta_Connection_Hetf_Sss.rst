Sss
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:STA<s>:CONNection:HETF:SSS

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:STA<s>:CONNection:HETF:SSS



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Sta.Connection.Hetf.Sss.SssCls
	:members:
	:undoc-members:
	:noindex: