Mcs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:STA<s>:CONNection:HETF:MCS

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:STA<s>:CONNection:HETF:MCS



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Sta.Connection.Hetf.Mcs.McsCls
	:members:
	:undoc-members:
	:noindex: