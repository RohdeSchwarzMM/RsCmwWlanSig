Station
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:STATion:SCONnection
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:STATion:CMODe

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:STATion:SCONnection
	CONFigure:WLAN:SIGNaling<instance>:CONNection:STATion:CMODe



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Station.StationCls
	:members:
	:undoc-members:
	:noindex: