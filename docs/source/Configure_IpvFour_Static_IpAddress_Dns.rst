Dns
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:IPVFour:STATic:IPADdress:DNS

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:IPVFour:STATic:IPADdress:DNS



.. autoclass:: RsCmwWlanSig.Implementations.Configure.IpvFour.Static.IpAddress.Dns.DnsCls
	:members:
	:undoc-members:
	:noindex: