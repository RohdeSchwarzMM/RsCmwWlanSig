Iloss
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:FADing:FSIMulator:ILOSs:LOSS

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:FADing:FSIMulator:ILOSs:LOSS



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Fading.Fsimulator.Iloss.IlossCls
	:members:
	:undoc-members:
	:noindex: