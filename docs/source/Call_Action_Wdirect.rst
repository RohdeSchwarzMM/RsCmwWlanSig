Wdirect
----------------------------------------





.. autoclass:: RsCmwWlanSig.Implementations.Call.Action.Wdirect.WdirectCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.call.action.wdirect.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Call_Action_Wdirect_Sconnection.rst