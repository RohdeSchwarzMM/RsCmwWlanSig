Third
----------------------------------------





.. autoclass:: RsCmwWlanSig.Implementations.Third.ThirdCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.third.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Third_State.rst