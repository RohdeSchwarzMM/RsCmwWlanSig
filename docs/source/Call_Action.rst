Action
----------------------------------------





.. autoclass:: RsCmwWlanSig.Implementations.Call.Action.ActionCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.call.action.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Call_Action_Station.rst
	Call_Action_Wdirect.rst
	Call_Action_Wps.rst