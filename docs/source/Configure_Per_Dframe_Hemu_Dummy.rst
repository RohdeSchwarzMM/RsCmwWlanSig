Dummy<Dummy>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr3
	rc = driver.configure.per.dframe.hemu.dummy.repcap_dummy_get()
	driver.configure.per.dframe.hemu.dummy.repcap_dummy_set(repcap.Dummy.Nr1)





.. autoclass:: RsCmwWlanSig.Implementations.Configure.Per.Dframe.Hemu.Dummy.DummyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.per.dframe.hemu.dummy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Per_Dframe_Hemu_Dummy_Mcs.rst