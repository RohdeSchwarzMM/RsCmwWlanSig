UphInfo
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: SENSe:WLAN:SIGNaling<instance>:STA<s>:HETBinfo:UPHinfo

.. code-block:: python

	SENSe:WLAN:SIGNaling<instance>:STA<s>:HETBinfo:UPHinfo



.. autoclass:: RsCmwWlanSig.Implementations.Sense.Sta.HetbInfo.UphInfo.UphInfoCls
	:members:
	:undoc-members:
	:noindex: