Hetf
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:HETF:TXP
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:HETF:TXEN
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:HETF:LDPC
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:HETF:APTXpower
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:HETF:MLTF
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:HETF:GILT
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:HETF:CHBW
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:HETF:CSR
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:HETF:NOFSymbols
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:HETF:TTYP

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:HETF:TXP
	CONFigure:WLAN:SIGNaling<instance>:CONNection:HETF:TXEN
	CONFigure:WLAN:SIGNaling<instance>:CONNection:HETF:LDPC
	CONFigure:WLAN:SIGNaling<instance>:CONNection:HETF:APTXpower
	CONFigure:WLAN:SIGNaling<instance>:CONNection:HETF:MLTF
	CONFigure:WLAN:SIGNaling<instance>:CONNection:HETF:GILT
	CONFigure:WLAN:SIGNaling<instance>:CONNection:HETF:CHBW
	CONFigure:WLAN:SIGNaling<instance>:CONNection:HETF:CSR
	CONFigure:WLAN:SIGNaling<instance>:CONNection:HETF:NOFSymbols
	CONFigure:WLAN:SIGNaling<instance>:CONNection:HETF:TTYP



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Hetf.HetfCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.hetf.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Hetf_SsTx.rst