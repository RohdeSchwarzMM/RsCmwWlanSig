UesInfo
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: SENSe:WLAN:SIGNaling<instance>:UESinfo:APSSid
	single: SENSe:WLAN:SIGNaling<instance>:UESinfo:RXTRigframe

.. code-block:: python

	SENSe:WLAN:SIGNaling<instance>:UESinfo:APSSid
	SENSe:WLAN:SIGNaling<instance>:UESinfo:RXTRigframe



.. autoclass:: RsCmwWlanSig.Implementations.Sense.UesInfo.UesInfoCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.uesInfo.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_UesInfo_Antenna.rst
	Sense_UesInfo_CmwAddress.rst