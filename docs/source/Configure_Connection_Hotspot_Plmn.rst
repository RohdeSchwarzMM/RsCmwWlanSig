Plmn<Plnm>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr5
	rc = driver.configure.connection.hotspot.plmn.repcap_plnm_get()
	driver.configure.connection.hotspot.plmn.repcap_plnm_set(repcap.Plnm.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:HOTSpot:PLMN<nr>

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:HOTSpot:PLMN<nr>



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Hotspot.Plmn.PlmnCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.hotspot.plmn.clone()