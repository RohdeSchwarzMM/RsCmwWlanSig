RfSettings
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:RFSettings:OCWidth
	single: CONFigure:WLAN:SIGNaling<instance>:RFSettings:FOFFset
	single: CONFigure:WLAN:SIGNaling<instance>:RFSettings:CHANnel
	single: CONFigure:WLAN:SIGNaling<instance>:RFSettings:BAND
	single: CONFigure:WLAN:SIGNaling<instance>:RFSettings:FREQuency
	single: CONFigure:WLAN:SIGNaling<instance>:RFSettings:NPINdex
	single: CONFigure:WLAN:SIGNaling<instance>:RFSettings:NPFRequency
	single: CONFigure:WLAN:SIGNaling<instance>:RFSettings:NPCHannel
	single: CONFigure:WLAN:SIGNaling<instance>:RFSettings:MLOFfset
	single: CONFigure:WLAN:SIGNaling<instance>:RFSettings:EPEPower
	single: CONFigure:WLAN:SIGNaling<instance>:RFSettings:TSRatio
	single: CONFigure:WLAN:SIGNaling<instance>:RFSettings:BOPower

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:RFSettings:OCWidth
	CONFigure:WLAN:SIGNaling<instance>:RFSettings:FOFFset
	CONFigure:WLAN:SIGNaling<instance>:RFSettings:CHANnel
	CONFigure:WLAN:SIGNaling<instance>:RFSettings:BAND
	CONFigure:WLAN:SIGNaling<instance>:RFSettings:FREQuency
	CONFigure:WLAN:SIGNaling<instance>:RFSettings:NPINdex
	CONFigure:WLAN:SIGNaling<instance>:RFSettings:NPFRequency
	CONFigure:WLAN:SIGNaling<instance>:RFSettings:NPCHannel
	CONFigure:WLAN:SIGNaling<instance>:RFSettings:MLOFfset
	CONFigure:WLAN:SIGNaling<instance>:RFSettings:EPEPower
	CONFigure:WLAN:SIGNaling<instance>:RFSettings:TSRatio
	CONFigure:WLAN:SIGNaling<instance>:RFSettings:BOPower



.. autoclass:: RsCmwWlanSig.Implementations.Configure.RfSettings.RfSettingsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.rfSettings.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_RfSettings_Antenna.rst
	Configure_RfSettings_Eattenuation.rst