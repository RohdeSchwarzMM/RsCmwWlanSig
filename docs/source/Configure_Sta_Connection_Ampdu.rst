Ampdu
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:STA<s>:CONNection:AMPDu

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:STA<s>:CONNection:AMPDu



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Sta.Connection.Ampdu.AmpduCls
	:members:
	:undoc-members:
	:noindex: