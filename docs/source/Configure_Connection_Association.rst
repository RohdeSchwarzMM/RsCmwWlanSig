Association
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:ASSociation:PREemption
	single: CONFigure:WLAN:SIGNaling<instance>:CONNection:ASSociation:STAPriority

.. code-block:: python

	CONFigure:WLAN:SIGNaling<instance>:CONNection:ASSociation:PREemption
	CONFigure:WLAN:SIGNaling<instance>:CONNection:ASSociation:STAPriority



.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Association.AssociationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.association.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Association_Disass.rst
	Configure_Connection_Association_Sta.rst