Wps
----------------------------------------





.. autoclass:: RsCmwWlanSig.Implementations.Call.Action.Wps.WpsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.call.action.wps.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Call_Action_Wps_Sconnection.rst