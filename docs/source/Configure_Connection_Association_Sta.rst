Sta<Station>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.configure.connection.association.sta.repcap_station_get()
	driver.configure.connection.association.sta.repcap_station_set(repcap.Station.Nr1)





.. autoclass:: RsCmwWlanSig.Implementations.Configure.Connection.Association.Sta.StaCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.connection.association.sta.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Connection_Association_Sta_MacReserve.rst